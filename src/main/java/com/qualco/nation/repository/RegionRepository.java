package com.qualco.nation.repository;

import com.qualco.nation.domain.Region;
import org.springframework.data.repository.CrudRepository;

public interface RegionRepository extends CrudRepository<Region, Integer> {
}
