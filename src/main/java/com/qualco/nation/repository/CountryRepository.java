package com.qualco.nation.repository;

import com.qualco.nation.domain.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends CrudRepository<Country, Integer> {


    @Query(value = "SELECT new com.qualco.nation.domain.CountryInfo(c.countryId, c.name, c.area, c.countryCodeTwo) FROM Country c order by c.name asc")
    List<CountryInfo> retrieveAllCountries();


    @Query(value = "SELECT new com.qualco.nation.domain.LanguageInfo(l.language) FROM Language l INNER JOIN CountryLanguage cl ON l.languageId = cl.language.languageId where cl.country.countryId = ?1")
    List<LanguageInfo> retrieveSpokenLanguagesById(int id);

    @Query(value = "SELECT new com.qualco.nation.domain.CountryBasicStat(c.name, c.countryCodeThree, cs.countryYearKey.year, cs.population, cs.gdp) FROM Country c INNER JOIN CountryStat cs ON c.countryId = cs.country.countryId order by c.name asc")
    List<CountryBasicStat> retrieveCountriesStats();

    @Query(value = "select new com.qualco.nation.domain.ContinentInfo(continent.name,region.name,country.name, stat.countryYearKey.year, stat.population, stat.gdp) " +
            "from Continent continent inner join Region region on continent.continentId = region.continent.continentId inner join " +
            "Country country on region.regionId = country.regionId inner join CountryStat stat on country.countryId = stat.country.countryId WHERE (?1 is null or region.regionId = ?1) and (?2 is null or stat.countryYearKey.year >= ?2)" +
            " and (?3 is null or stat.countryYearKey.year <= ?3)")
    List<ContinentInfo> retrieveStatsByCriteria(@Param("region") Integer region, @Param("yearFrom") Integer yearFrom, @Param("yearTo") Integer yearTo);


}
