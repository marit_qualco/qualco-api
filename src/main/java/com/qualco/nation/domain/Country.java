package com.qualco.nation.domain;


import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * This is Country entity corresponding to countries table
 *
 */
@Entity
@Table(name = "countries", indexes = {@Index(name = "country_code2", columnList = "country_code2", unique = true),
        @Index(name = "country_code3", columnList = "country_code3", unique = true),
        @Index(name = "region_id", columnList = "region_id")})
public class Country {

    @Id
    @Column(name = "country_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int countryId;

    @Column(length = 50)
    private String name;

    @Column( precision = 10, scale = 2)
    private BigDecimal area;

    @Column( name="national_day")
    private Date nationalDay;

    @Column(columnDefinition="char(2)", name = "country_code2")
    private String countryCodeTwo;

    @Column(columnDefinition="char(3)", name = "country_code3")
    private String countryCodeThree;

    @ManyToOne
    @JoinColumn(name="REGION_ID", foreignKey=@ForeignKey(name = "countries_ibfk_1"))
    private Region regionId;

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<CountryLanguage> countryLanguages;

    @OneToMany(mappedBy = "country", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<CountryStat> countryStats;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public Date getNationalDay() {
        return nationalDay;
    }

    public void setNationalDay(Date nationalDay) {
        this.nationalDay = nationalDay;
    }

    public String getCountryCodeTwo() {
        return countryCodeTwo;
    }

    public void setCountryCodeTwo(String countryCodeTwo) {
        this.countryCodeTwo = countryCodeTwo;
    }

    public String getCountryCodeThree() {
        return countryCodeThree;
    }

    public void setCountryCodeThree(String countryCodeThree) {
        this.countryCodeThree = countryCodeThree;
    }

    public Region getRegionId() {
        return regionId;
    }

    public void setRegionId(Region regionId) {
        this.regionId = regionId;
    }

    public Set<CountryLanguage> getCountryLanguages() {
        return countryLanguages;
    }

    public void setCountryLanguages(Set<CountryLanguage> countryLanguages) {
        this.countryLanguages = countryLanguages;
    }

    public Set<CountryStat> getCountryStats() {
        return countryStats;
    }

    public void setCountryStats(Set<CountryStat> countryStats) {
        this.countryStats = countryStats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return countryId == country.countryId && name.equals(country.name) && area.equals(country.area) && nationalDay.equals(country.nationalDay) && countryCodeTwo.equals(country.countryCodeTwo) && countryCodeThree.equals(country.countryCodeThree) && regionId.equals(country.regionId) && countryLanguages.equals(country.countryLanguages) && countryStats.equals(country.countryStats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryId, name, area, nationalDay, countryCodeTwo, countryCodeThree, regionId, countryLanguages, countryStats);
    }
}
