package com.qualco.nation.domain;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * This is a DTO used to return only the specified values from tables continents, regions, countries and country_stats
 *
 */
public class ContinentInfo {


    private String continentName;

    private String regionName;

    private String countryName;

    private int year;

    private int population;

    private BigDecimal gdp;

    public String getContinentName() {
        return continentName;
    }

    public void setContinentName(String continentName) {
        this.continentName = continentName;
    }

    public String getRegionName() {
        return regionName;
    }

    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public BigDecimal getGdp() {
        return gdp;
    }

    public void setGdp(BigDecimal gdp) {
        this.gdp = gdp;
    }

    public ContinentInfo() {
    }

    public ContinentInfo(String continentName, String regionName, String countryName, int year, int population, BigDecimal gdp) {
        this.continentName = continentName;
        this.regionName = regionName;
        this.countryName = countryName;
        this.year = year;
        this.population = population;
        this.gdp = gdp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContinentInfo that = (ContinentInfo) o;
        return year == that.year && population == that.population && continentName.equals(that.continentName) && regionName.equals(that.regionName) && countryName.equals(that.countryName) && gdp.equals(that.gdp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(continentName, regionName, countryName, year, population, gdp);
    }
}
