package com.qualco.nation.domain;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;


/**
 * This is CountryStat entity corresponding to country_stats table
 *
 */
@Entity
@Table(name = "country_stats")
public class CountryStat {

    @EmbeddedId
    private CountryYearKey countryYearKey;


    @ManyToOne
    @JoinColumn(name = "country_id",insertable = false, updatable = false, foreignKey=@ForeignKey(name = "countries_stats_ibfk_1"))
    private Country country;

    private int population;

    @Column( precision = 15, scale = 0)
    private BigDecimal gdp;

    public CountryYearKey getCountryYearKey() {
        return countryYearKey;
    }

    public void setCountryYearKey(CountryYearKey countryYearKey) {
        this.countryYearKey = countryYearKey;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public BigDecimal getGdp() {
        return gdp;
    }

    public void setGdp(BigDecimal gdp) {
        this.gdp = gdp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryStat that = (CountryStat) o;
        return population == that.population && countryYearKey.equals(that.countryYearKey) && country.equals(that.country) && gdp.equals(that.gdp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryYearKey, country, population, gdp);
    }
}
