package com.qualco.nation.domain;


import java.math.BigDecimal;
import java.util.Objects;


/**
 * This is a DTO used to return only the specified values from countries table
 *
 */
public class CountryInfo {

    private int id;

    private String name;

    private BigDecimal area;

    private String countryCodeTwo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getArea() {
        return area;
    }

    public void setArea(BigDecimal area) {
        this.area = area;
    }

    public String getCountryCodeTwo() {
        return countryCodeTwo;
    }

    public void setCountryCodeTwo(String countryCodeTwo) {
        this.countryCodeTwo = countryCodeTwo;
    }

    public CountryInfo(int id, String name, BigDecimal area, String countryCodeTwo) {
        this.id = id;
        this.name = name;
        this.area = area;
        this.countryCodeTwo = countryCodeTwo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryInfo that = (CountryInfo) o;
        return id == that.id && name.equals(that.name) && area.equals(that.area) && countryCodeTwo.equals(that.countryCodeTwo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, area, countryCodeTwo);
    }

    public CountryInfo() {
    }
}
