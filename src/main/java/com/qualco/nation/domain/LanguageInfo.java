package com.qualco.nation.domain;


/**
 * This is a DTO used to simplify Frontend mapping
 *
 */
public class LanguageInfo {

    private String languageName;

    public String getLanguageName() {
        return languageName;
    }

    public void setLanguageName(String languageName) {
        this.languageName = languageName;
    }

    public LanguageInfo(String languageName) {
        this.languageName = languageName;
    }

    public LanguageInfo() {
    }
}
