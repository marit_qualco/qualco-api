package com.qualco.nation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Objects;

/**
 * This is Region entity corresponding to regions table
 *
 */

@Entity
@Table(name = "regions", indexes = {@Index(name = "continent_id", columnList = "continent_id")})
public class Region {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "region_id")
    private int regionId;

    @Column(length = 100)
    private String name;

    @ManyToOne
    @JoinColumn(name="continent_id",insertable = false, updatable = false, foreignKey=@ForeignKey(name = "regions_ibfk_1"))
    @JsonIgnore
    public Continent continent;

    public int getRegionId() {
        return regionId;
    }

    public void setRegionId(int regionId) {
        this.regionId = regionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Region region = (Region) o;
        return regionId == region.regionId && name.equals(region.name) && continent.equals(region.continent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(regionId, name, continent);
    }
}
