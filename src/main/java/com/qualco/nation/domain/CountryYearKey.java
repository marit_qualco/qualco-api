package com.qualco.nation.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


/**
 * This class represents the primary composite key of country_stats table
 *
 */
@Embeddable
public class CountryYearKey implements Serializable {


    @Column( name= "country_id")
    private int countryId;

    private int year;


    public CountryYearKey() {
    }

    public CountryYearKey(int countryId, int year) {
        this.countryId = countryId;
        this.year = year;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryYearKey that = (CountryYearKey) o;
        return countryId == that.countryId && year == that.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryId, year);
    }
}
