package com.qualco.nation.domain;

import java.math.BigDecimal;
import java.util.Objects;


/**
 * This is a DTO used to return only the specified values from tables countries and country_stats
 *
 */
public class CountryBasicStat {

    private String name;

    private String countryCodeThree;

    private int year;

    private int population;

    private BigDecimal gdp;

    public CountryBasicStat(String name, String countryCodeThree, int year, int population, BigDecimal gdp) {
        this.name = name;
        this.countryCodeThree = countryCodeThree;
        this.year = year;
        this.population = population;
        this.gdp = gdp;
    }

    public CountryBasicStat() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountryCodeThree() {
        return countryCodeThree;
    }

    public void setCountryCodeThree(String countryCodeThree) {
        this.countryCodeThree = countryCodeThree;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public BigDecimal getGdp() {
        return gdp;
    }

    public void setGdp(BigDecimal gdp) {
        this.gdp = gdp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryBasicStat that = (CountryBasicStat) o;
        return year == that.year && population == that.population && name.equals(that.name) && countryCodeThree.equals(that.countryCodeThree) && gdp.equals(that.gdp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, countryCodeThree, year, population, gdp);
    }
}
