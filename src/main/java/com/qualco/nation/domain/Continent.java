package com.qualco.nation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


/**
 * This is continent entity corresponding to continents table
 *
 */

@Entity
@Table(name = "continents")
public class Continent {

    @Id
    @Column(name = "continent_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int continentId;

    @Column(length = 255)
    private String name;

   @OneToMany(mappedBy = "continent", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
   @JsonIgnore
    private List<Region> regions;


    public int getContinentId() {
        return continentId;
    }

    public void setContinentId(int continentId) {
        this.continentId = continentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Continent continent = (Continent) o;
        return continentId == continent.continentId && name.equals(continent.name) && regions.equals(continent.regions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(continentId, name, regions);
    }
}
