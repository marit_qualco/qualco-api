package com.qualco.nation.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


/**
 * This class represents the primary composite key of country_languages table
 *
 */
@Embeddable
public class CountryLanguagesKey implements Serializable {

    @Column(name = "country_id")
    int countryId;

    @Column(name = "language_id")
    int languageId;

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getLanguageId() {
        return languageId;
    }

    public void setLanguageId(int languageId) {
        this.languageId = languageId;
    }

    public CountryLanguagesKey(int countryId, int languageId) {
        this.countryId = countryId;
        this.languageId = languageId;
    }

    public CountryLanguagesKey() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryLanguagesKey that = (CountryLanguagesKey) o;
        return countryId == that.countryId && languageId == that.languageId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(countryId, languageId);
    }
}
