package com.qualco.nation.domain;



import javax.persistence.*;
import java.util.Objects;


/**
 * This is CountryLanguage entity corresponding to country_languages table
 *
 */
@Entity
@Table(name = "country_languages", indexes = {@Index(name = "language_id", columnList = "language_id")})
public class CountryLanguage {

    @EmbeddedId
    CountryLanguagesKey id;

    @ManyToOne
    @MapsId("countryId")
    @JoinColumn(name = "country_id", foreignKey=@ForeignKey(name = "countries_languages_ibfk_1"))
    Country country;

    @ManyToOne
    @MapsId("languageId")
    @JoinColumn(name = "language_id", foreignKey=@ForeignKey(name = "countries_languages_ibfk_2"))
    Language language;

    @Column(columnDefinition = "TINYINT(1)")
    private byte official;

    public CountryLanguagesKey getId() {
        return id;
    }

    public void setId(CountryLanguagesKey id) {
        this.id = id;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public byte getOfficial() {
        return official;
    }

    public void setOfficial(byte official) {
        this.official = official;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CountryLanguage that = (CountryLanguage) o;
        return official == that.official && id.equals(that.id) && country.equals(that.country) && language.equals(that.language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, country, language, official);
    }
}
