package com.qualco.nation.service;

import com.qualco.nation.repository.RegionRepository;
import com.qualco.nation.domain.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class RegionsServiceImpl implements RegionService {

    /**
     * Repository containing region data
     */
    @Autowired
    private RegionRepository regionRepository;


    /**
     *  Retrieves data from RegionRepository
     *
     * @return a {@link List< Region >}
     */
    @Override
    @Transactional(readOnly = true)
    public List<Region> retrieveAllRegions() {
        return (List<Region>) regionRepository.findAll();
    }
}
