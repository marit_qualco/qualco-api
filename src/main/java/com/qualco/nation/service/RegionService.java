package com.qualco.nation.service;

import com.qualco.nation.domain.Region;

import java.util.List;

public interface RegionService {


    List<Region> retrieveAllRegions();
}
