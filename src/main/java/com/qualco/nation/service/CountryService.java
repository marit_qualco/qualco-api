package com.qualco.nation.service;

import com.qualco.nation.domain.ContinentInfo;
import com.qualco.nation.domain.CountryBasicStat;
import com.qualco.nation.domain.CountryInfo;
import com.qualco.nation.domain.LanguageInfo;;

import java.util.List;

public interface CountryService {

    List<CountryInfo> retrieveAllCountries();

    List<LanguageInfo> retrieveSpokenLanguagesById(int id);

    List<CountryBasicStat> retrieveCountriesStats();

    List<ContinentInfo> retrieveStatsByCriteria(Integer region, Integer yearFrom, Integer yearTo);

}
