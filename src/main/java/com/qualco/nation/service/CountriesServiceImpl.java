package com.qualco.nation.service;

import com.qualco.nation.domain.*;
import com.qualco.nation.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CountriesServiceImpl implements CountryService {


    /**
     * Repository containing helper methods to get country data
     */
    @Autowired
    private CountryRepository countryRepository;

    /**
     * Retrieves all Countries from CountryRepository
     *
     * @return a {@link List< CountryInfo >}
     */
    @Override
    @Transactional(readOnly = true)
    public List<CountryInfo> retrieveAllCountries() {
        return countryRepository.retrieveAllCountries();
    }

    /**
     * Retrieves Spoken Languages from CountryRepository using the provided criteria
     *
     * @param id
     * @return a {@link List<String>}
     */
    @Override
    @Transactional(readOnly = true)
    public List<LanguageInfo> retrieveSpokenLanguagesById(int id) {
        return countryRepository.retrieveSpokenLanguagesById(id);
    }

    /**
     * Retrieves statistics from CountryRepository
     *
     * @return  a {@link List< CountryBasicStat >}
     */
    @Override
    @Transactional(readOnly = true)
    public List<CountryBasicStat> retrieveCountriesStats() {
        return countryRepository.retrieveCountriesStats();
    }

    /**
     * Retrieves statistics per continent from CountryRepository using the provided criteria
     *
     * @param region
     * @param yearFrom
     * @param yearTo
     * @return a {@link List< ContinentInfo >}
     */
    @Override
    @Transactional(readOnly = true)
    public List<ContinentInfo> retrieveStatsByCriteria(Integer region, Integer yearFrom, Integer yearTo) {
        return countryRepository.retrieveStatsByCriteria(region, yearFrom, yearTo);
    }




}
