package com.qualco.nation.controller;

import com.qualco.nation.exception.DateCriteriaException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ApplicationExceptionHandler {



    @ExceptionHandler(DateCriteriaException.class)
    public ResponseEntity<String> handleException(DateCriteriaException e) {
        return new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleException(IllegalArgumentException e) {
        return new ResponseEntity<String>(e.getClass().toString() + " " + e.getMessage() , HttpStatus.BAD_REQUEST);
    }

}
