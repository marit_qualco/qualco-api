package com.qualco.nation.controller;


import com.qualco.nation.domain.*;
import com.qualco.nation.exception.DateCriteriaException;
import com.qualco.nation.service.CountryService;
import com.qualco.nation.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Resource containing various endpoints exposing countries related information\\
 *
 * Note: CrossOrigin was added only for DEV purposes
 */

@RestController
//@CrossOrigin
public class AssessmentController {

    /**
     * Service containing methods to get countries data
     */
    @Autowired
    private CountryService countryService;

    /**
     * Service containing the methods to get region data
     */
    @Autowired
    private RegionService regionService;


    /**
     * Retrieves all Countries
     *
     * @return a {@link List< CountryInfo >}
     */
    @GetMapping(value="/countries", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<CountryInfo>> retrieveAllCountries() {
        return new ResponseEntity(countryService.retrieveAllCountries(),HttpStatus.OK);
    }

    /**
     * Retrieves all the spoken languages for the provided Id
     *
     * @param countryId the country_id of countries table
     * @return a {@link List<LanguageInfo>}
     */
    @GetMapping(value="/languages/{countryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<LanguageInfo>> retrieveSpokenLanguagesById(@PathVariable int countryId) {
        return new ResponseEntity(countryService.retrieveSpokenLanguagesById(countryId),HttpStatus.OK);
    }


    /**
     * Retrieves all Countries Statistics
     *
     * @return a {@link List< CountryBasicStat >}
     */
    @GetMapping(value="/countriesstats", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<CountryBasicStat>> retrieveCountriesStats() {
        return new ResponseEntity(countryService.retrieveCountriesStats(),HttpStatus.OK);
    }


    /**
     * Retrieves all the Statistics for the provided criteria
     *
     * @param region the Id of the regions table
     * @param yearFrom date filter passed from the UI
     * @param yearTo date filter passed from the UI
     * @return  a {@link List< ContinentInfo >}
     */
    @GetMapping(value="/continents", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<ContinentInfo>> retrieveStatsByCriteria(@RequestParam(name = "region",required = false) Integer region, @RequestParam(name = "yearFrom",required = false) Integer yearFrom, @RequestParam(name = "yearTo", required = false) Integer yearTo) {
        if(yearFrom != null && yearTo != null){
            if (yearFrom > yearTo) {
                throw new DateCriteriaException("yearTo should not exceed yearFrom");
            }
        }

        return new ResponseEntity(countryService.retrieveStatsByCriteria(region, yearFrom, yearTo),HttpStatus.OK);
    }


    /**
     * Retrieves all the regions
     *
     * @return a {@link List< Region >}
     */
    @GetMapping(value="/regions", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<List<Region>> retrieveAllRegions() {
        return new ResponseEntity(regionService.retrieveAllRegions(),HttpStatus.OK);
    }





}
