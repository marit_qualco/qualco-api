package com.qualco.nation.exception;

public class DateCriteriaException extends RuntimeException{

    public DateCriteriaException(String message) {
        super(message);
    }
}
