package com.qualco.nation;

import com.qualco.nation.controller.AssessmentController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


import static org.assertj.core.api.Assertions.assertThat;

/**
 * Various test methods to verify controllers & repositories behavior
 */
@SpringBootTest
class AssessmentControllerTests {


	@Autowired
	private AssessmentController controller;


	@Test
	void testRetrieveAllCountriesController() {
		assertThat(controller.retrieveAllCountries().getStatusCode().value()).isEqualTo(200);
		assertThat(controller.retrieveAllCountries().getBody().size()).isEqualTo(239);
	}

	@Test
	void testRetrieveSpokenLanguagesById() {
		assertThat(controller.retrieveSpokenLanguagesById(86).getBody().size()).isEqualTo(2);

	}

	@Test
	void testRetrieveCountriesStats() {
		assertThat(controller.retrieveCountriesStats().getBody().get(3378).getYear()).isEqualTo(2014);
	}

	@Test
	void testRetrieveStatsByCriteria() {
		assertThat(controller.retrieveStatsByCriteria(4, 2012,2014).getBody().size()).isEqualTo(36);
	}

	@Test
	void testRetrieveAllRegions() {
		assertThat(controller.retrieveAllRegions().getBody().size()).isEqualTo(25);
	}
}
