# Qualco Assesment

## Author
Maritina Ioannou - maritioannou@gmail.com

[Spring Boot](http://projects.spring.io/spring-boot/) application.

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.qualco.nation.Application` class from your IDE.

Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```

## Run Integration tests locally

There are several ways to run a Spring Boot application on your local machine. One way is to execute the `main` method in the `com.qualco.nation.AssessmentControllerTests` class from your IDE.

Alternatively you can use

```shell
mvn test
```